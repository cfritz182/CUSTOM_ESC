#include "imu.h"
#include "ch.h"
#include "hal.h"
#include "mpu6050.h"
#include <math.h>
#include "datatypes.h"

/* Settings for I2C*/
static const I2CConfig i2cfg = {
    OPMODE_I2C,
    100000,
    STD_DUTY_CYCLE
};
// Creation of IMU
IMU mpu;
// ThreadSize
static THD_WORKING_AREA(ic2_1,1024);

/* Thread for the IMU over I2C*/
static THD_FUNCTION(i2c_thread, arg){
  (void)arg;

  chRegSetThreadName("IC2");
  i2cStart(&I2CD1,&i2cfg);
  chThdSleepMilliseconds(100);

  MPU6050(MPU6050_ADDRESS_AD0_LOW);
  MPUinitialize();
  chThdSleepMilliseconds(10);
// Initialization of IMU
  mpu.ax=0;
  mpu.gx=0;
  mpu.ay=0;
  mpu.gy=0;
  mpu.az=0;
  mpu.gz=0;
  mpu.gyro_sens=131;
  mpu.accel_sens=16384;
  mpu.Axy_r=0.0;
  mpu.Axz_r=0.0;
  mpu.Ayz_r=0.0;
//Sampling Time
  float dt=0.01;

  double ax=0.0;
  double ay=0.0;
  double az=0.0;
  double ax_n=0.0;
  double ay_n=0.0;
  double az_n=0.0;

  double gx=0.0;
  double gy=0.0;
  double gz=0.0;

  double R=0.0;

  double SUM_AXZ=0.0;
  double SUM_AYZ=0.0;
  double SUM_AXY=0.0;

  double Axy=0.0;
  double Ayz=0.0;
  double Axz=0.0;

// Offset for Gyroscopes
  for (int i=0;i<100;i++){
  chThdSleepMilliseconds(10);
  MPUgetMotion6(&mpu.ax,&mpu.ay,&mpu.az,&mpu.gx,&mpu.gy,&mpu.gz);
// Getting Raw data
  ax=(double)mpu.ax/mpu.accel_sens;
  ay=(double)mpu.ay/mpu.accel_sens;
  az=(double)mpu.az/mpu.accel_sens;
  gx=(double)mpu.gx/mpu.gyro_sens;
  gy=(double)mpu.gy/mpu.gyro_sens;
  gz=(double)mpu.gz/mpu.gyro_sens;
  R=sqrt(ax*ax+ay*ay+az*az); // Normalization
  ax_n=ax/R;
  ay_n=ay/R;
  az_n=az/R;
  SUM_AYZ+=RAD2DEG*atan2(ay_n,az_n);
  SUM_AXZ+=RAD2DEG*atan2(ax_n,az_n);
  SUM_AXY+=RAD2DEG*atan2(ay_n,ax_n);
  }
  mpu.Axy_r=SUM_AXY/100.0;
  mpu.Axz_r=SUM_AXZ/100.0;
  mpu.Ayz_r=SUM_AYZ/100.0;

  while(1){
  chThdSleepMilliseconds(10);
  MPUgetMotion6(&mpu.ax,&mpu.ay,&mpu.az,&mpu.gx,&mpu.gy,&mpu.gz);
  ax=(double)mpu.ax/mpu.accel_sens;
  ay=(double)mpu.ay/mpu.accel_sens;
  az=(double)mpu.az/mpu.accel_sens;
  gx=(double)mpu.gx/mpu.gyro_sens;
  gy=(double)mpu.gy/mpu.gyro_sens;
  gz=(double)mpu.gz/mpu.gyro_sens;
  R=sqrt(ax*ax+ay*ay+az*az); //Normalization

  ax_n=ax/R;
  ay_n=ay/R;
  az_n=az/R;
  Ayz=RAD2DEG*atan2(ay_n,az_n);
  Axz=RAD2DEG*atan2(ax_n,az_n);
  Axy=RAD2DEG*atan2(-ay_n,-ax_n);
  if(ay_n <=0.1 || ax_n <=0.1){ // If ay, ax<=0.1 just gz data
      mpu.Axy_r=mpu.Axy_r-gz*dt;
    }
  else{
      mpu.Axy_r=0.98*(mpu.Axy_r-gz*dt)+0.02*Ayz;
    }
  mpu.Ayz_r=0.98*(mpu.Ayz_r+gx*dt)+0.02*Ayz;
  mpu.Axz_r=0.98*(mpu.Axz_r-gy*dt)+0.02*Axz;
  }
}

/* Initialization of the IMU Thread
 * USING IC21 on
 * PB8->SCL
 * PB9->SDA              */
bool imu_init(void) {
  i2cInit();
  palSetPadMode(GPIOB,8,
          PAL_MODE_ALTERNATE(4) |
          PAL_STM32_OTYPE_OPENDRAIN |
          PAL_STM32_OSPEED_MID1 |
          PAL_STM32_PUDR_PULLUP);

  palSetPadMode(GPIOB, 9,
          PAL_MODE_ALTERNATE(4) |
          PAL_STM32_OTYPE_OPENDRAIN |
          PAL_STM32_OSPEED_MID1 |
          PAL_STM32_PUDR_PULLUP);
  chThdCreateStatic(ic2_1, sizeof(ic2_1), NORMALPRIO, i2c_thread, NULL);
  return true;
}

float get_Axy(void){
  return (float)mpu.Axy_r;
}

float get_Axz(void){
  return (float)mpu.Axz_r;
}

float get_Ayz(void){
  return (float)mpu.Ayz_r;
}

IMU get_IMU(void){
  return mpu;
}
