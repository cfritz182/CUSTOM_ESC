#ifndef IMU_H_
#define IMU_H_

#include "datatypes.h"

#define RAD2DEG 54.2958

bool imu_init(void);

float get_Axy(void);
float get_Axz(void);
float get_Ayz(void);
IMU get_IMU(void);

#endif /*IMU_H_*/
